import socket # for socket
 
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print ("Socket successfully created")
except socket.error as err:
    print ("socket creation failed with error %s" %(err))


# connecting to the server
s.sendto("200:c".encode(), ("34.125.245.207", 3000))

print (s.recv(1024).decode())
# close the connection
s.close()   