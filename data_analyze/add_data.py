# %%
import pandas as pd
import sys
sys.path.append('../')
WINS = 0
LOSSES = 1
DIFFERENCE = 2
LAST = 3
# %%
games_df = pd.read_csv('big_games_results.csv')
# %%
games_df.insert(0, 'Year', games_df['Visitor'].map(lambda x: x[:4]))
games_df.insert(0, 'Hm_Wins', 0)
games_df.insert(0, 'Vis_Wins', 0)
games_df.insert(0, 'Hm_Losses', 0)
games_df.insert(0, 'Vis_Losses', 0)
games_df.insert(0, 'Hm_Difference', 0)
games_df.insert(0, 'Vis_Difference', 0)
games_df.insert(0, 'Hm_Last_game', 0)
games_df.insert(0, 'Vis_Last_game', 0)
# %%
year = ''
for index in games_df.index:
    if index % 1000 == 0:
        print(index)
    date = games_df.loc[index, 'days_from_start']
    if games_df.loc[index, 'Year'] != year:
        year = games_df.loc[index, 'Year']
        balance = dict()
    home_group = games_df.loc[index, 'Home'][4:]
    visitor_group = games_df.loc[index, 'Visitor'][4:]
    if home_group not in balance.keys():
        # (wins, losses, balance, days from last game)
        balance[home_group] = [0, 0, 0, date - 15]
    if visitor_group not in balance.keys():
        balance[visitor_group] = [0, 0, 0, date - 15]
    result = games_df.loc[index, 'Result']
    if result > 0:
        balance[home_group][WINS] += 1
        balance[visitor_group][LOSSES] += 1
    else:
        balance[home_group][LOSSES] += 1
        balance[visitor_group][WINS] += 1
    games_df.loc[index, 'Hm_Last_game'] = date - balance[home_group][LAST]
    games_df.loc[index, 'Vis_Last_game'] = date - balance[visitor_group][LAST]
    balance[home_group][DIFFERENCE] += result
    balance[home_group][LAST] = date
    balance[visitor_group][DIFFERENCE] -= result
    balance[visitor_group][LAST] = date
    games_df.loc[index, 'Hm_Wins'] = balance[home_group][WINS]
    games_df.loc[index, 'Vis_Wins'] = balance[visitor_group][WINS]
    games_df.loc[index, 'Hm_Losses'] = balance[home_group][LOSSES]
    games_df.loc[index, 'Vis_Losses'] = balance[visitor_group][LOSSES]
    games_df.loc[index, 'Hm_Difference'] = balance[home_group][DIFFERENCE]
    games_df.loc[index, 'Vis_Difference'] = balance[visitor_group][DIFFERENCE]

# %%
games_df = games_df[games_df['Vis_Difference'].notna()]
games_df = games_df[games_df['Hm_Difference'].notna()]

games_df.to_csv("big_games_balance.csv")

# %%
