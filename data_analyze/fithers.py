# %%
import pandas as pd
final_df = pd.read_csv("tables/final_tables")
final_df = final_df[['vs_Losses', 'vs_Wins', 'vs_Difference', 'vs_Last_game','Vis_SRS', 'Vis_L', 'Vis_W', 'Vis_rating',
'hm_Losses', 'hm_Wins', 'hm_Difference', 'hm_Last_game','Hm_SRS', 'Hm_L', 'Hm_W', 'Hm_rating', 'days_from_start', 'Result']]
#%%
for fither in [ 'W', 'L', 'SRS']:
    print(fither)
    final_df.insert(0, fither, final_df['Hm_'+fither] - final_df['Vis_'+fither]) 
    final_df.drop('Hm_' + fither, axis=1, inplace=True)
    final_df.drop('Vis_' + fither, axis=1, inplace=True)
# %%
for fither in ['Difference', 'Losses', 'Wins']:
    print(fither)
    final_df.insert(0, fither, final_df['hm_'+fither] - final_df['vs_'+fither]) 
    final_df.drop('hm_' + fither, axis=1, inplace=True)
    final_df.drop('vs_' + fither, axis=1, inplace=True)
#%%
final_df.to_csv('../Lasso_table_balance.csv', index=False)
# %%
